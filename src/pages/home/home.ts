import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ParamgetPage} from '../paramget/paramget';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  onButtonClicked(nama,umur)
  {
    console.log('nama'+nama+' umur'+umur);
    this.navCtrl.push(ParamgetPage,{nama:nama,umur:umur});
  }

}
