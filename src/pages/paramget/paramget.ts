import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ParamgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paramget',
  templateUrl: 'paramget.html',
})
export class ParamgetPage implements OnInit {

  nama: string;
  umur: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.nama=this.navParams.get['nama'];
    //   this.umur=this.navParams.get['umur'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParamgetPage');
  }

  ngOnInit()
    {
      this.nama=this.navParams.get('nama');
      this.umur=this.navParams.get('umur');
      console.log(this.navParams.get['umur']+this.navParams.get['nama']);
      this.nama
    }

}
