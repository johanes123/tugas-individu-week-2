import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParamgetPage } from './paramget';

@NgModule({
  declarations: [
    ParamgetPage,
  ],
  imports: [
    IonicPageModule.forChild(ParamgetPage),
  ],
})
export class ParamgetPageModule {}
